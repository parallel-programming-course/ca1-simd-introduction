#include "stdio.h"
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include "x86intrin.h"

#define U8  ((unsigned char)0x01)
#define I8  ((unsigned char)0x11)
#define U16 ((unsigned char)0x02)
#define I16 ((unsigned char)0x12)
#define U32 ((unsigned char)0x03)
#define I32 ((unsigned char)0x13)
#define U64 ((unsigned char)0x04)
#define I64 ((unsigned char)0x14)

typedef union {
	__m128i 			int128;
	
	unsigned char		m128_u8[16];
	signed char			m128_i8[16];

	unsigned short		m128_u16[8];
	signed short		m128_i16[8];

	unsigned int		m128_u32[4];
	signed int		    m128_i32[4];

	unsigned long		m128_u64[2];
	signed long		    m128_i64[2];

} intVec;

typedef union {
	__m128 			float128;	
	float		m128_32[4];

} floatVec;


void cpuid(int CPUInfo[4], int InfoType){
	__asm__ __volatile__(
		"cpuid":
	    "=a" (CPUInfo[0]),
		"=b" (CPUInfo[1]),
		"=c" (CPUInfo[2]),
		"=d" (CPUInfo[3]) :
		"a" (InfoType)
		);
}

void cpu_type(void) {
	FILE *fp = fopen("/proc/cpuinfo", "r");
	size_t n = 0;
	char *line = NULL;
	while (getline(&line, &n, fp) > 0) {
		if (strstr(line, "model name") || strstr(line, "cpu cores")) {
			printf("%s\n", line);
			break;
		}
 	}
	free(line);
	fclose(fp);
}

void simd_info(void) {

	int info[4];
	bool MMX   = false;
	bool SSE   = false;
	bool SSE2  = false;
	bool SSE3  = false;
	bool AES   = false;
	bool SSE41 = false;
	bool SSE42 = false;
	bool AVX   = false;
	bool AVX2  = false;
	bool SHA   = false;

	cpuid(info, 0x00000001);

	MMX   = (info[3] & ((int)1 << 23)) != 0;

	SSE   = (info[3] & ((int)1 << 25)) != 0;
	SSE2  = (info[3] & ((int)1 << 26)) != 0;
	SSE3  = (info[2] & ((int)1 << 0))  != 0;
	AES   = (info[2] & ((int)1 << 25)) != 0;
	SSE41 = (info[2] & ((int)1 << 19)) != 0;
	SSE42 = (info[2] & ((int)1 << 20)) != 0;

	AVX   = (info[2] & ((int)1 << 28)) != 0;

	cpuid(info, 0x80000000);
	if (info[0] >= 0x00000007){
		cpuid(info, 0x00000007);
		AVX2   = (info[1] & ((int)1 <<  5)) != 0;
		SHA    = (info[1] & ((int)1 << 29)) != 0;
	}

	printf("%s\n", MMX   ? "MMX   Supported" : "MMX   NOT Supported");
	printf("%s\n", SSE   ? "SSE   Supported" : "SSE   NOT Supported");
	printf("%s\n", SSE2  ? "SSE2  Supported" : "SSE2  NOT Supported");
	printf("%s\n", SSE3  ? "SSE3  Supported" : "SSE3  NOT Supported");
	printf("%s\n", SSE41 ? "SSE41 Supported" : "SSE41 NOT Supported");
	printf("%s\n", SSE42 ? "SSE42 Supported" : "SSE42 NOT Supported");
	printf("%s\n", AES   ? "AES   Supported" : "AES   NOT Supported");
	printf("%s\n", SHA   ? "SHA   Supported" : "SHA   NOT Supported");
	printf("%s\n", AVX   ? "AVX   Supported" : "AVX   NOT Supported");
	printf("%s\n", AVX2  ? "AVX2  Supported" : "AVX2  NOT Supported");	
}


void print_int_vector (__m128i x, unsigned char type)
{
	intVec tmp;
	tmp.int128 = x;
	printf ("[");
	switch(type){
		case U8: 
			for (int i=15; i>0; i--) 
				printf ("%X, ", tmp.m128_u8[i]);
			printf ("%X]\n\n", tmp.m128_u8[0]);
			break;
		case I8: 
			for (int i=15; i>0; i--) 
				printf ("%X, ", tmp.m128_i8[i]);
			printf ("%X]\n\n", tmp.m128_i8[0]);
			break;
		case U16: 
			for (int i=7; i>0; i--) 
				printf ("%X, ", tmp.m128_u16[i]);
			printf ("%X]\n\n", tmp.m128_u16[0]);
			break;
		case I16: 
			for (int i=7; i>0; i--) 
				printf ("%X, ", tmp.m128_i16[i]);
			printf ("%X]\n\n", tmp.m128_i16[0]);
			break;
		case U32: 
			for (int i=3; i>0; i--) 
				printf ("%X, ", tmp.m128_u32[i]);
			printf ("%X]\n\n", tmp.m128_u32[0]);
			break;
		case I32: 
			for (int i=3; i>0; i--) 
				printf ("%X, ", tmp.m128_i32[i]);
			printf ("%X]\n\n", tmp.m128_i32[0]);
			break;
		case U64: 
			for (int i=1; i>0; i--) 
				printf ("%lX, ", tmp.m128_u64[i]);
			printf ("%lX]\n\n", tmp.m128_u64[0]);
			break;
		case I64: 
			for (int i=1; i>0; i--) 		
				printf ("%lX, ", tmp.m128_i64[i]);
			printf ("%lX]\n\n", tmp.m128_i64[0]);
			break;
		default:
			break;
	}
		
}



void test_SIMD_blocks_int(void) {
	unsigned char unsignedIntArray [16] = {	0X00, 0X11, 0X22, 0X33, 0X44, 0X55, 0X66, 0X77,
								0X88, 0X99, 0XAA, 0XBB, 0XCC, 0XDD, 0XEE, 0XFF};

	signed char signedIntArray [16] = {	0X00, 0X11, 0X22, 0X33, 0X44, 0X55, 0X66, 0X77,
								'\x88', '\x99', '\xAA', '\xBB', '\xCC', '\xDD', '\xEE', '\xFF'};

	__m128i a;
	__m128i b;
	a = _mm_load_si128((const __m128i*)unsignedIntArray);
	b = _mm_load_si128((const __m128i*)signedIntArray);

	printf ("Unsigned byte: ");
	print_int_vector (a, U8);

	printf ("Signed byte: ");
	print_int_vector (b, I8);

	printf ("Unsigned word: ");
	print_int_vector (a, U16);

	printf ("Signed word: ");
	print_int_vector (b, I16);
	
	printf ("Unsigned double word: ");
	print_int_vector (a, U32);

	printf ("Signed double word: ");
	print_int_vector (b, I32);

	printf ("Unsigned quad word: ");
	print_int_vector (a, U64);

	printf ("Signed quad word: ");
	print_int_vector (b, I64);
}

void print_spfp_vector(__m128 a) {
	floatVec f;
	f.float128 = a;
	printf ("Floating point: ");
	printf("[");
	for (int i=3; i>0; i--)
		printf("%f, ", f.m128_32[i]);

	printf("%f]\n", f.m128_32[0]);
}

void test_SIMD_blocks_float(void) {

	unsigned int floatArray [4] = {0X40100000, 0X40B4CCCD, 0X47C35000, 0X4309BAE1};
								// 2.25, 5.65, 100000, 137.73
	
	__m128 a;
	a = _mm_load_ps((const float*)floatArray);
	print_spfp_vector(a);
}

int main(void)
{
	printf("MirHamed Jafarzadeh Asl: 810096008\n");
	printf("Seyede Mehrnaz Shamsabadi: 810196493\n\n");

	printf("Part 1\n");
	cpu_type();
	simd_info();

	printf("\n------------------------------------------------\n\nPart 2\n");
	test_SIMD_blocks_int();
	test_SIMD_blocks_float();
	return 0;
}
